#!/bin/bash

pool_miners=miners.json
index_file=index.html
sedded=sedded
cat $pool_miners | sed 's/:{\"hashrate1m\":\"/<td>/g; s/\",\"hashrate5m\":\"/<\/td><td>/g; s/\",\"hashrate1hr\":\"/<\/td><td>/g; s/\",\"hashrate1d":\"/<\/td><td>/g; s/\",\"hashrate7d":\"/<\/td><td>/g; s/\",\"lastshare\":/<\/td><td>/g; s/,\"workers\":/<\/td><td>/g; s/,\"shares":/<\/td><td>/g; s/,\"bestshare\":/<\/td><td>/g; s/,\"bestshare_alltime\":/<\/td><td>/g; s/,\"lns":/<\/td><td>/g; s/,\"luck":/<\/td><td>/g; s/,\"accumulated":/<\/td><td>/g; s/,\"postponed":/<\/td><td>/g; s/,\"herp":/<\/td><td>/g; s/,\"derp":/<\/td><td>/g; s/,\"time":/<\/td><td>/g; s/[{}]//g' > $sedded

miner=$(while read miner_line; do echo "<tr><td> $miner_line </td></tr>"; done <$sedded)


touch $index_file

cat > $index_file << EOF

<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="https://unpkg.com/purecss@2.0.6/build/pure-min.css" integrity="sha384-Uu6IeWbM+gzNVXJcM9XV3SohHtmWE+3VGi496jvgX1jyvDTXfdK+rfZc8C1Aehk5" crossorigin="anonymous">
    <meta http-equiv="refresh" content="30">
    <meta name="description" content="Ergon active miners list with statistics.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Ergon Pool Miners</title>
    <style type="text/css">
        body{   
            margin:20px; padding: 20px; background:#F0F0F0;
            }
    </style>
  </head>
  <body>
    <h1>Active Miners</h1>
    <table class="pure-table">
        <thead>
            <tr>
                <th>Worker</th>
                <th>Hashrate1m</th>
                <th>Hashrate5m</th>
                <th>Hashrate1hr</th>
                <th>Hashrate1d</th>
                <th>Hashrate7d</th>
                <th>Lastshare</th>
                <th>Workers</th>
                <th>Shares</th>
                <th>Bestshare</th>
                <th>Bestshare_alltime</th>
                <th>Lns</th>
                <th>Luck</th>
                <th>Accumulated</th>
                <th>Postponed</th>
                <th>Herp</th>
                <th>Derp</th>
                <th>Time</th>
            </tr>
        </thead>
    <!-- Here goes the miners list -->
<!-- START -->
    $miner
<!-- END -->
    </table>
  </body>
</html>

EOF
